/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage3_process.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <okosiako@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/25 10:19:52 by dkovalch          #+#    #+#             */
/*   Updated: 2017/11/01 20:42:59 by okosiako         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 42sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "error.h"
#include "string.h"
#include "grammar.h"
#include <termios.h>
#include <sys/wait.h>
#include "structs_global.h"
#include "line_processor.h"
#include "command_processor.h"
#include "line_processor_internal.h"

/*
**	Line processor - stage 3
**	This function runs given command list, and waits for them to end.
**
**	Return value:
**		stdout from the command list
*/

static void	run(t_command *head, const int fds[2])
{
	pid_t	pid;

	if ((pid = fork()) == 0)
	{
		dup2(fds[STDOUT_FILENO], STDOUT_FILENO);
		close(fds[STDIN_FILENO]);
		close(fds[STDOUT_FILENO]);
		cp_process_commands(head);
		exit(EXIT_SUCCESS);
	}
	else
	{
		close(fds[STDOUT_FILENO]);
		waitpid(pid, NULL, 0);
		tcsetattr(g_global->terminal_fd, TCSADRAIN, &(g_global->terminal_mine));
	}
}

/*
**	Line processor - stage 3
**	This function reads given fd.
**
**	Return value:
**		stdout from the command list
*/

static char	*read_it(const int fd)
{
	char	*rt;
	char	*buf;

	buf = NULL;
	rt = ft_strdup("\'");
	while (get_next_line(fd, &buf))
	{
		rt = ft_strjoin_ultimate(&rt, &buf);
		rt = ft_strjoin_del_first(&rt, "\n");
	}
	rt = ft_strjoin_del_first(&rt, "\'");
	return (rt);
}

/*
**	Line processor - stage 3
**	This function runs the argument as a command, and substitues it with
**	the stdout's output.
*/

void		lp_s3_process_backqts(char **arg)
{
	t_command		*head;
	char			*output;
	int				fds[2];

	head = NULL;
	output = ft_strdup(*arg);
	str_trim_qts(&output);
	ft_strdel(arg);
	if (lp_process_line(&head, output))
	{
		if (pipe(fds) != 0)
			err_print(ERR_CRT_PIPE, output);
		else
		{
			run(head, fds);
			*arg = read_it(fds[STDIN_FILENO]);
			close(fds[STDIN_FILENO]);
		}
	}
	ft_strdel(&output);
	st_cmd_delete_list(&head);
}

/*
**	Line processor - stage 3
**	This function iterates over arguments, calling proper functions, depending
**	on the quotation marks presence.
**
**	Return value:
**		true	-	success
**		false	-	fail, with error message already printed
*/

bool		lp_s3_process_argument(t_command *const token,
										ssize_t *const arg_no)
{
	bool	no_error;

	no_error = true;
	if (!gr_is_sqt(token->args[*arg_no], 0))
		lp_s4_replace_vars(&(token->args[*arg_no]));
	if (!gr_is_qt(token->args[*arg_no], 0))
	{
		lp_s4_replace_homes(&(token->args[*arg_no]));
		lp_s4_wildcard(token, *arg_no);
	}
	if (*arg_no >= 0)
		no_error = lp_s4_heredocs(token, arg_no);
	if (*arg_no >= 0 && no_error)
	{
		if (!gr_is_sqt(token->args[*arg_no], 0))
			lp_s4_replace_bslashes(&(token->args[*arg_no]));
		no_error = lp_s4_redirection(token, arg_no);
		if (*arg_no >= 0)
			str_trim_qts(&(token->args[*arg_no]));
	}
	return (no_error);
}
