/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage0_history.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <okosiako@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 21:19:53 by okosiako          #+#    #+#             */
/*   Updated: 2017/11/05 14:21:59 by okosiako         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 42sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "error.h"
#include "string.h"
#include "grammar.h"
#include "structs_global.h"
#include "line_processor_internal.h"

/*
** Line processor - stage 0
** This function replace the '!N' patterned part of cmd_line with
** appropriate history line.
** If history[N] not exist - error message is printed.
*/

static bool		replace_history_to_val(char **cmd_line, size_t *ct)
{
	const t_history_list	*elem;
	char					*temp;
	int						history_index;

	history_index = ft_atoi(&((*cmd_line)[*ct + 1]));
	elem = st_hst_get_by_index(history_index);
	if (!history_index || !elem)
	{
		err_print(&(*cmd_line)[*ct], ERR_WRONG_HISTORY);
		return (false);
	}
	temp = ft_strsub(*cmd_line, 0, *ct);
	(*ct)++;
	if ((*cmd_line)[*ct] == '+' || (*cmd_line)[*ct] == '-')
		(*ct)++;
	while(ft_isdigit((*cmd_line)[*ct]))
		(*ct)++;
	temp = ft_strjoin_del_first(&temp, elem->line);
	temp = ft_strjoin_del_first(&temp, &((*cmd_line)[*ct]));
	ft_strdel(cmd_line);
	*cmd_line = temp;
	return (true);
}

/*
** Line processor - stage 0
** This function dispatch with next processes:
** 1 - find '!' and check if it's a totem
** 2 - check the requared history index, if it's not valid,
**			just replace a '!'
**
** [TODO] Think about bool returning function so we can:
**					1) print modified cmd_line if there were replacing
**					2) don't add not valid command (aka '!0') to history
*/

bool			lp_s0_replace_history(char **cmd_line)
{
	size_t	ct;

	ct = 0;
	while(cmd_line && *cmd_line && (*cmd_line)[ct])
	{
		if (gr_is_sqt(*cmd_line, ct))
			ct += str_skip_qts(&(*cmd_line)[ct]) - (&(*cmd_line)[ct]);
		else if (gr_is_history(*cmd_line, ct) &&
					!replace_history_to_val(cmd_line, &ct))
			return (false);
		else
			ct++;
	}
	return (true);
}
