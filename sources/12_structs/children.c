/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   children.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/04 21:02:08 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/27 08:48:13 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 42sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "structs_global.h"
#include "structs_children.h"

/*
**	Structures - children
**	This function adds given child structure to the list, and updates first and
**	second defaults.
*/

void		st_chl_push_back(t_child_list *const child)
{
	t_child_list	*temp;

	child->is_default = true;
	child->is_2default = false;
	temp = g_global->children;
	while (temp)
	{
		if (temp->is_2default)
			temp->is_2default = false;
		if (temp->is_default)
		{
			temp->is_default = false;
			temp->is_2default = true;
		}
		if (temp->next == NULL)
			break ;
		temp = temp->next;
	}
	if (!temp)
		g_global->children = child;
	else
		temp->next = child;
}

/*
**	Structures - children
**	This function deletes all elements of a single child structure.
*/

static void	st_chl_delete_one(t_child_list *item)
{
	ft_memdel((void**)&(item->state));
	ft_memdel((void**)&(item->command));
	ft_memdel((void**)&(item->raw_data));
}

/*
**	Structures - children
**	This function sets the default and 2default flags to the found pointers.
**	First it checks if we have a situation when there is only 2default, meaning
**	that we need to make our substitue child main one, and found a
**	new substitute. Then it sets proper flags, and after that it looks for
**	missing default child(ren), and sets the flags again, in case we found them.
*/

static void	set_defaults(t_child_list *def[2], t_child_list *biggest_num[2])
{
	if (def[0] == NULL && def[1] != NULL)
	{
		def[0] = def[1];
		def[1] = NULL;
	}
	if (def[0] != NULL)
		def[0]->is_default = true;
	if (def[1] != NULL)
		def[1]->is_2default = true;
	if (def[0] == NULL && biggest_num[0] && !biggest_num[0]->is_2default)
		biggest_num[0]->is_default = true;
	if (def[0] == NULL && biggest_num[1] && !biggest_num[1]->is_2default)
		biggest_num[1]->is_default = true;
	if (def[1] == NULL && biggest_num[0] && !biggest_num[0]->is_default)
		biggest_num[0]->is_2default = true;
	if (def[1] == NULL && biggest_num[1] && !biggest_num[1]->is_default)
		biggest_num[1]->is_2default = true;
	if (def[0] != NULL)
		def[0]->is_default = true;
	if (def[1] != NULL)
		def[1]->is_2default = true;
}

/*
**	Structures - children
**	This function looks for the defaults in the list,
**	clears default flags and updates them if necessary.
*/

static void	update_defaults(void)
{
	t_child_list	*temp;
	t_child_list	*def[2];
	t_child_list	*biggest_num[2];

	def[0] = NULL;
	def[1] = NULL;
	temp = g_global->children;
	biggest_num[0] = temp;
	biggest_num[1] = temp;
	while (temp)
	{
		def[0] = temp->is_default ? temp : def[0];
		def[1] = temp->is_2default ? temp : def[1];
		biggest_num[1] = temp->number > biggest_num[0]->number
			? biggest_num[0] : biggest_num[1];
		biggest_num[0] = temp->number > biggest_num[0]->number
			? temp : biggest_num[0];
		temp->is_2default = false;
		temp->is_default = false;
		temp = temp->next;
	}
	set_defaults(def, biggest_num);
}

/*
**	Structures - children
**	This function deletes child structure with given pid,
**	and reconnects the list.
*/

void		st_chl_delete(const pid_t pid_in)
{
	t_child_list	*prev;
	t_child_list	*temp;

	prev = NULL;
	temp = g_global->children;
	while (temp)
	{
		if (temp->pid == pid_in)
		{
			if (prev)
				prev->next = temp->next;
			else
				g_global->children = temp->next;
			st_chl_delete_one(temp);
			ft_memdel((void**)&temp);
			break ;
		}
		else
		{
			prev = temp;
			temp = temp->next;
		}
	}
	update_defaults();
}
